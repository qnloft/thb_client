#thb_client

JAVA8新推出了javafx，一直关注，但是一直没有机会在项目中运用。正好最近有一个机会，可以让我体会一下javafx的与众不同。
> - **项目背景**：某物流公司需要一批机器用作取单机，主要是通过扫描取单人的二维码，进行单据信息打印。PC主要是windows系统普通电脑、触摸屏、打印机是HP1020黑白激光打印机、扫码机，全部设备封装成一个整机。


> - **设计思路**：将javafx程序封装成EXE软件，安装部署在每台机器上，然后通过HTTP发起请求，调取服务器接口，验证和拉取数据。


`LoginController`是项目的主入口，`PrintController`是打印页面。`application.css`是打印页面展示效果的CSS，`application_print.css`是打印页面打印效果的CSS。lib目录下是该项目所用到的第三方jar包。`MainController`是测试打印页面的类。

项目配置：需要项目设置java build path里面引入全内部第三方jar包和jdk，然后运行`LoginController`

[项目介绍传送门](https://my.oschina.net/qnloft/blog/995786)
[项目导出jar包传送门](https://my.oschina.net/qnloft/blog/995787)

