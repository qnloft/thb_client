package com.erdos.thb.tools;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpClient {

	public static String gethttpResponse(String uri) {
		String result = null;
		try {
			// 创建httpclient对象
			CloseableHttpClient client = HttpClients.createDefault();
			
			// 根据地址获取请求
			HttpGet get = new HttpGet(uri);// 这里发送get请求
			//请求超时时间的设置
			RequestConfig requestConfig = RequestConfig.custom()    
			        .setConnectTimeout(5000).setConnectionRequestTimeout(1000)    
			        .setSocketTimeout(5000).build();
			get.setConfig(requestConfig);

			// 执行请求
			CloseableHttpResponse httpResponse = client.execute(get);
			try {
				HttpEntity entity = httpResponse.getEntity();
				
				if (null != entity) {
					result = EntityUtils.toString(entity);
				}
			} finally {
				httpResponse.close();
			}

			// 判断网络连接状态码是否正常(0--200都数正常)
			// if (get.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			// result= EntityUtils.toString(response.getEntity(),"utf-8");
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		// ....result是用户信息,站内业务以及具体的json转换这里也不写了...
	}

}
