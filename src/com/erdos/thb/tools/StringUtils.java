package com.erdos.thb.tools;

public class StringUtils {

	public static String isEmpty(String value) {
        if (value == null || value.length() == 0) {
            return "";
        }

        return value;
    }
	
	public static boolean isEmpty(Object value) {
        if (value == null) {
            return false;
        }

        return true;
    }
}
