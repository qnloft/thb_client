package com.erdos.thb.model;

public class LoginResultData {

	private String code;
	private String message;
	private String Data;
	public String getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	public String getData() {
		return Data;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setData(String data) {
		Data = data;
	}
	
	
}
