package com.erdos.thb.model;

public class DeliveryAddress {

	private String complete;
	private String key;
	private String value;

	public String getComplete() {
		return complete;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public void setComplete(String complete) {
		this.complete = complete;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
