package com.erdos.thb.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * 计划单实体
 * @author fuxingdong 
 * @date 2017年5月12日
 */
public class PlanOrder {
	
	private String copanyName;
	
    private Integer id;

    private String planCode;
    
    private Integer companyId;

    private Integer goodsId;

    private String goodsName;

    private String goodsModel;

    private SysDict goodsType;

    private String goodsSpec;

    private String goodsNum;

    private Integer planTruckNum;
    
    private BigDecimal planWeightNum;

    private Integer logisticsNum;

    private Integer driverNum;

    private Integer deliverCompanyId;

    private Integer deliverId;

    private String deliveryCompanyName;

    private String deliveryCompanyAddressDetail;

    private DeliveryAddress deliveryCompanyAddressP;

    private DeliveryAddress deliveryCompanyAddressC;

    private DeliveryAddress deliveryCompanyAddressA;

    private DeliveryAddress deliveryCompanyAddressS;

    private String deliveryLinkMan;

    private String deliveryLinkMobile;

    private BigDecimal transFee;

    private Integer receiverCompanyId;

    private String deliveryTime;

    private String planType;

    private String remark;

    private SysDict planStatus;

    private String receiverCompanyName;

    private String receiverCompanyAddressDetail;
    
    private DeliveryAddress receiverCompanyAddressP;

    private DeliveryAddress receiverCompanyAddressC;

    private DeliveryAddress receiverCompanyAddressA;

    private DeliveryAddress receiverCompanyAddressS;

    private String receiverLinkMan;

    private String receiverLinkMobile;
    
    private String infoFeeLogistics;

    private String infoFeeBuyer;
    
    private BigDecimal deviation;

    private String createTime;

    private Integer createUser;

    private String updateTime;

    private Integer updateUser;

    private String delFlag;
    
    private List<PlanDistLogistics> planDistLogisticss=new ArrayList<PlanDistLogistics>();
    
    private List<PlanDistDriver> planDistDrivers=new ArrayList<PlanDistDriver>();
    
    
    
    
	public BigDecimal getDeviation() {
		return deviation;
	}

	public void setDeviation(BigDecimal deviation) {
		this.deviation = deviation;
	}

	public List<PlanDistDriver> getPlanDistDrivers() {
		return planDistDrivers;
	}

	public void setPlanDistDrivers(List<PlanDistDriver> planDistDrivers) {
		this.planDistDrivers = planDistDrivers;
	}

	public List<PlanDistLogistics> getPlanDistLogisticss() {
		return planDistLogisticss;
	}

	public void setPlanDistLogisticss(List<PlanDistLogistics> planDistLogisticss) {
		this.planDistLogisticss = planDistLogisticss;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode == null ? null : planCode.trim();
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    public String getGoodsModel() {
        return goodsModel;
    }

    public void setGoodsModel(String goodsModel) {
        this.goodsModel = goodsModel == null ? null : goodsModel.trim();
    }


    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec == null ? null : goodsSpec.trim();
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum == null ? null : goodsNum.trim();
    }

    public Integer getPlanTruckNum() {
        return planTruckNum;
    }

    public void setPlanTruckNum(Integer planTruckNum) {
        this.planTruckNum = planTruckNum;
    }

    public Integer getLogisticsNum() {
        return logisticsNum;
    }

    public void setLogisticsNum(Integer logisticsNum) {
        this.logisticsNum = logisticsNum;
    }

    public Integer getDriverNum() {
        return driverNum;
    }

    public void setDriverNum(Integer driverNum) {
        this.driverNum = driverNum;
    }

    public Integer getDeliverCompanyId() {
        return deliverCompanyId;
    }

    public void setDeliverCompanyId(Integer deliverCompanyId) {
        this.deliverCompanyId = deliverCompanyId;
    }

    public Integer getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(Integer deliverId) {
        this.deliverId = deliverId;
    }

    public String getDeliveryCompanyName() {
        return deliveryCompanyName;
    }

    public void setDeliveryCompanyName(String deliveryCompanyName) {
        this.deliveryCompanyName = deliveryCompanyName == null ? null : deliveryCompanyName.trim();
    }

    public String getDeliveryCompanyAddressDetail() {
        return deliveryCompanyAddressDetail;
    }

    public void setDeliveryCompanyAddressDetail(String deliveryCompanyAddressDetail) {
        this.deliveryCompanyAddressDetail = deliveryCompanyAddressDetail == null ? null : deliveryCompanyAddressDetail.trim();
    }

  
    public String getDeliveryLinkMan() {
        return deliveryLinkMan;
    }

    public void setDeliveryLinkMan(String deliveryLinkMan) {
        this.deliveryLinkMan = deliveryLinkMan == null ? null : deliveryLinkMan.trim();
    }

    public String getDeliveryLinkMobile() {
        return deliveryLinkMobile;
    }

    public void setDeliveryLinkMobile(String deliveryLinkMobile) {
        this.deliveryLinkMobile = deliveryLinkMobile == null ? null : deliveryLinkMobile.trim();
    }

    public BigDecimal getTransFee() {
        return transFee;
    }

    public void setTransFee(BigDecimal transFee) {
        this.transFee = transFee;
    }

    public Integer getReceiverCompanyId() {
        return receiverCompanyId;
    }

    public void setReceiverCompanyId(Integer receiverCompanyId) {
        this.receiverCompanyId = receiverCompanyId;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType == null ? null : planType.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }


    public String getReceiverCompanyName() {
        return receiverCompanyName;
    }

    public void setReceiverCompanyName(String receiverCompanyName) {
        this.receiverCompanyName = receiverCompanyName == null ? null : receiverCompanyName.trim();
    }

    public String getReceiverCompanyAddressDetail() {
        return receiverCompanyAddressDetail;
    }

    public void setReceiverCompanyAddressDetail(String receiverCompanyAddressDetail) {
        this.receiverCompanyAddressDetail = receiverCompanyAddressDetail == null ? null : receiverCompanyAddressDetail.trim();
    }

    public DeliveryAddress getReceiverCompanyAddressP() {
		return receiverCompanyAddressP;
	}

	public DeliveryAddress getReceiverCompanyAddressC() {
		return receiverCompanyAddressC;
	}

	public DeliveryAddress getReceiverCompanyAddressA() {
		return receiverCompanyAddressA;
	}

	public DeliveryAddress getReceiverCompanyAddressS() {
		return receiverCompanyAddressS;
	}

	public void setReceiverCompanyAddressP(DeliveryAddress receiverCompanyAddressP) {
		this.receiverCompanyAddressP = receiverCompanyAddressP;
	}

	public void setReceiverCompanyAddressC(DeliveryAddress receiverCompanyAddressC) {
		this.receiverCompanyAddressC = receiverCompanyAddressC;
	}

	public void setReceiverCompanyAddressA(DeliveryAddress receiverCompanyAddressA) {
		this.receiverCompanyAddressA = receiverCompanyAddressA;
	}

	public void setReceiverCompanyAddressS(DeliveryAddress receiverCompanyAddressS) {
		this.receiverCompanyAddressS = receiverCompanyAddressS;
	}

	public String getReceiverLinkMan() {
        return receiverLinkMan;
    }

    public void setReceiverLinkMan(String receiverLinkMan) {
        this.receiverLinkMan = receiverLinkMan == null ? null : receiverLinkMan.trim();
    }

    public String getReceiverLinkMobile() {
        return receiverLinkMobile;
    }

    public void setReceiverLinkMobile(String receiverLinkMobile) {
        this.receiverLinkMobile = receiverLinkMobile == null ? null : receiverLinkMobile.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    
	public String getInfoFeeLogistics() {
		return infoFeeLogistics;
	}

	public void setInfoFeeLogistics(String infoFeeLogistics) {
		this.infoFeeLogistics = infoFeeLogistics;
	}

	
	public String getInfoFeeBuyer() {
		return infoFeeBuyer;
	}

	public void setInfoFeeBuyer(String infoFeeBuyer) {
		this.infoFeeBuyer = infoFeeBuyer;
	}

	public BigDecimal getPlanWeightNum() {
		return planWeightNum;
	}

	public void setPlanWeightNum(BigDecimal planWeightNum) {
		this.planWeightNum = planWeightNum;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	
	public SysDict getGoodsType() {
		return goodsType;
	}

	public DeliveryAddress getDeliveryCompanyAddressP() {
		return deliveryCompanyAddressP;
	}

	public DeliveryAddress getDeliveryCompanyAddressC() {
		return deliveryCompanyAddressC;
	}

	public DeliveryAddress getDeliveryCompanyAddressA() {
		return deliveryCompanyAddressA;
	}

	public DeliveryAddress getDeliveryCompanyAddressS() {
		return deliveryCompanyAddressS;
	}

	public SysDict getPlanStatus() {
		return planStatus;
	}

	public void setGoodsType(SysDict goodsType) {
		this.goodsType = goodsType;
	}

	public void setDeliveryCompanyAddressP(DeliveryAddress deliveryCompanyAddressP) {
		this.deliveryCompanyAddressP = deliveryCompanyAddressP;
	}

	public void setDeliveryCompanyAddressC(DeliveryAddress deliveryCompanyAddressC) {
		this.deliveryCompanyAddressC = deliveryCompanyAddressC;
	}

	public void setDeliveryCompanyAddressA(DeliveryAddress deliveryCompanyAddressA) {
		this.deliveryCompanyAddressA = deliveryCompanyAddressA;
	}

	public void setDeliveryCompanyAddressS(DeliveryAddress deliveryCompanyAddressS) {
		this.deliveryCompanyAddressS = deliveryCompanyAddressS;
	}

	public void setPlanStatus(SysDict planStatus) {
		this.planStatus = planStatus;
	}


	public String getCopanyName() {
		return copanyName;
	}

	public void setCopanyName(String copanyName) {
		this.copanyName = copanyName;
	}

	@Override
	public String toString() {
		return "PlanOrder [id=" + id + ", planCode=" + planCode + ", goodsId="
				+ goodsId + ", goodsName=" + goodsName + ", goodsModel="
				+ goodsModel + ", goodsType=" + goodsType + ", goodsSpec="
				+ goodsSpec + ", goodsNum=" + goodsNum + ", planTruckNum="
				+ planTruckNum + ", planWeightNum=" + planWeightNum
				+ ", logisticsNum=" + logisticsNum + ", driverNum=" + driverNum
				+ ", deliverCompanyId=" + deliverCompanyId + ", deliverId="
				+ deliverId + ", deliveryCompanyName=" + deliveryCompanyName
				+ ", deliveryCompanyAddressDetail="
				+ deliveryCompanyAddressDetail + ", deliveryCompanyAddressP="
				+ deliveryCompanyAddressP + ", deliveryCompanyAddressC="
				+ deliveryCompanyAddressC + ", deliveryCompanyAddressA="
				+ deliveryCompanyAddressA + ", deliveryCompanyAddressS="
				+ deliveryCompanyAddressS + ", deliveryLinkMan="
				+ deliveryLinkMan + ", deliveryLinkMobile="
				+ deliveryLinkMobile + ", transFee=" + transFee
				+ ", receiverCompanyId=" + receiverCompanyId
				+ ", deliveryTime=" + deliveryTime + ", planType=" + planType
				+ ", remark=" + remark + ", planStatus=" + planStatus
				+ ", receiverCompanyName=" + receiverCompanyName
				+ ", receiverCompanyAddressDetail="
				+ receiverCompanyAddressDetail + ", receiverCompanyAddressP="
				+ receiverCompanyAddressP + ", receiverCompanyAddressC="
				+ receiverCompanyAddressC + ", receiverCompanyAddressA="
				+ receiverCompanyAddressA + ", receiverCompanyAddressS="
				+ receiverCompanyAddressS + ", receiverLinkMan="
				+ receiverLinkMan + ", receiverLinkMobile="
				+ receiverLinkMobile + ", infoFeeLogistics=" + infoFeeLogistics
				+ ", infoFeeBuyer=" + infoFeeBuyer + ", deviation=" + deviation
				+ ", createTime=" + createTime + ", createUser=" + createUser
				+ ", updateTime=" + updateTime + ", updateUser=" + updateUser
				+ ", delFlag=" + delFlag + ", planDistLogisticss="
				+ planDistLogisticss + ", planDistDrivers=" + planDistDrivers
				+ "]";
	}

	
}