package com.erdos.thb.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PlanDistLogistics {
    private Integer id;

    private String logisticsCode;

    private Integer planId;

    private Integer logisticsCompanyId;

    private Integer truckNum;

    private BigDecimal transFee;

    private String logisticsStatus;

    private String logisticsCompanyName;

    private String logisticsCompanyLinkMan;

    private String logisticsCompanyLinkMobile;

    private LocalDateTime logisticsCompanyLoadTime;

    private LocalDateTime createTime;

    private Integer createUser;

    private LocalDateTime updateTime;

    private Integer updateUser;

    private String delFlag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode == null ? null : logisticsCode.trim();
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getLogisticsCompanyId() {
        return logisticsCompanyId;
    }

    public void setLogisticsCompanyId(Integer logisticsCompanyId) {
        this.logisticsCompanyId = logisticsCompanyId;
    }

    public Integer getTruckNum() {
        return truckNum;
    }

    public void setTruckNum(Integer truckNum) {
        this.truckNum = truckNum;
    }

    public BigDecimal getTransFee() {
        return transFee;
    }

    public void setTransFee(BigDecimal transFee) {
        this.transFee = transFee;
    }

    public String getLogisticsStatus() {
        return logisticsStatus;
    }

    public void setLogisticsStatus(String logisticsStatus) {
        this.logisticsStatus = logisticsStatus == null ? null : logisticsStatus.trim();
    }

    public String getLogisticsCompanyName() {
        return logisticsCompanyName;
    }

    public void setLogisticsCompanyName(String logisticsCompanyName) {
        this.logisticsCompanyName = logisticsCompanyName == null ? null : logisticsCompanyName.trim();
    }

    public String getLogisticsCompanyLinkMan() {
        return logisticsCompanyLinkMan;
    }

    public void setLogisticsCompanyLinkMan(String logisticsCompanyLinkMan) {
        this.logisticsCompanyLinkMan = logisticsCompanyLinkMan == null ? null : logisticsCompanyLinkMan.trim();
    }

    public String getLogisticsCompanyLinkMobile() {
        return logisticsCompanyLinkMobile;
    }

    public void setLogisticsCompanyLinkMobile(String logisticsCompanyLinkMobile) {
        this.logisticsCompanyLinkMobile = logisticsCompanyLinkMobile == null ? null : logisticsCompanyLinkMobile.trim();
    }

    public LocalDateTime getLogisticsCompanyLoadTime() {
        return logisticsCompanyLoadTime;
    }

    public void setLogisticsCompanyLoadTime(LocalDateTime logisticsCompanyLoadTime) {
        this.logisticsCompanyLoadTime = logisticsCompanyLoadTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

	public PlanDistLogistics() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}