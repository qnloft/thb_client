package com.erdos.thb.model;

public class ResultData {

	private String code;
	private String message;
	private PlanDistDriver Data;
	public String getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PlanDistDriver getData() {
		return Data;
	}
	public void setData(PlanDistDriver data) {
		Data = data;
	}
	
	
}
