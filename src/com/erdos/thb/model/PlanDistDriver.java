package com.erdos.thb.model;

import java.math.BigDecimal;

public class PlanDistDriver {
	private PlanOrder planOrder;
	
	private String driverLicense;
	
    private Integer id;

    private String driverDistCode;

    private Integer planId;

    private Integer planDistLogisticsId;
    
    private Integer planDistCompanyId;

    private String planDistCompanyName;
    
    private Integer printCount;

    private Integer driverId;

    private String plateNo;

    private SysDict goodsType;

    private SysDict truckType;

    private String driverName;

    private String driverMobile;

    private BigDecimal transFee;

    private BigDecimal infoFee;

    private String driverDistLoadTime;

    private SysDict distDriverStatus;

    private String distDriverFrom;
    
    private String linkMobile;
    
    private String accountType;

    private String remark;

    private Integer createUser;

    private String createTime;

    private Integer updateUser;

    private String updateTime;

    private String delFlag;

    
    public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLinkMobile() {
		return linkMobile;
	}

	public void setLinkMobile(String linkMobile) {
		this.linkMobile = linkMobile;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDriverDistCode() {
        return driverDistCode;
    }

    public void setDriverDistCode(String driverDistCode) {
        this.driverDistCode = driverDistCode == null ? null : driverDistCode.trim();
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getPlanDistLogisticsId() {
        return planDistLogisticsId;
    }

    public void setPlanDistLogisticsId(Integer planDistLogisticsId) {
        this.planDistLogisticsId = planDistLogisticsId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo == null ? null : plateNo.trim();
    }


    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName == null ? null : driverName.trim();
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile == null ? null : driverMobile.trim();
    }

    public BigDecimal getTransFee() {
        return transFee;
    }

    public void setTransFee(BigDecimal transFee) {
        this.transFee = transFee;
    }

    public BigDecimal getInfoFee() {
        return infoFee;
    }

    public void setInfoFee(BigDecimal infoFee) {
        this.infoFee = infoFee;
    }

    public String getDriverDistLoadTime() {
        return driverDistLoadTime;
    }

    public void setDriverDistLoadTime(String driverDistLoadTime) {
        this.driverDistLoadTime = driverDistLoadTime;
    }


    public String getDistDriverFrom() {
        return distDriverFrom;
    }

    public void setDistDriverFrom(String distDriverFrom) {
        this.distDriverFrom = distDriverFrom == null ? null : distDriverFrom.trim();
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

	public PlanOrder getPlanOrder() {
		return planOrder;
	}

	public void setPlanOrder(PlanOrder planOrder) {
		this.planOrder = planOrder;
	}

	public Integer getPlanDistCompanyId() {
		return planDistCompanyId;
	}

	public void setPlanDistCompanyId(Integer planDistCompanyId) {
		this.planDistCompanyId = planDistCompanyId;
	}

	public Integer getPrintCount() {
		return printCount;
	}

	public void setPrintCount(Integer printCount) {
		this.printCount = printCount;
	}

    public String getPlanDistCompanyName() {
        return planDistCompanyName;
    }

    public void setPlanDistCompanyName(String planDistCompanyName) {
        this.planDistCompanyName = planDistCompanyName;
    }

	public SysDict getGoodsType() {
		return goodsType;
	}

	public SysDict getTruckType() {
		return truckType;
	}

	public SysDict getDistDriverStatus() {
		return distDriverStatus;
	}

	public void setGoodsType(SysDict goodsType) {
		this.goodsType = goodsType;
	}

	public void setTruckType(SysDict truckType) {
		this.truckType = truckType;
	}

	public void setDistDriverStatus(SysDict distDriverStatus) {
		this.distDriverStatus = distDriverStatus;
	}

	public String getDriverLicense() {
		return driverLicense;
	}

	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}
	
    
}