package com.erdos.thb.model;

public class SysDict {

	private String description;
    private String label;
    private String type;
    private String value;
	public String getDescription() {
		return description;
	}
	public String getLabel() {
		return label;
	}
	public String getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setValue(String value) {
		this.value = value;
	}
    
    
}
