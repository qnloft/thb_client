package com.erdos.thb.controller;

import java.net.InetAddress;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.erdos.thb.model.PlanDistDriver;
import com.erdos.thb.model.PlanOrder;
import com.erdos.thb.model.ResultData;
import com.erdos.thb.tools.IntenetTool;
import com.erdos.thb.tools.QrCodeUtil;
import com.erdos.thb.tools.StringUtils;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.print.JobSettings;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.Printer.MarginType;
import javafx.print.PrinterJob;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class PrintController extends Application {
	final Button btn = new Button();

	final BorderPane printBorderPane = new BorderPane();

	final int anchorPaneStartLeftWidth = 0;
	final int anchorPaneStartTopWidth = 0;

	final String url = "";
	// final String url = "http://10.144.10.101:8080/thb/admin/qrcode/";
	private String code;
	private Stage primaryStage;
	private Scene scene;
	private String companyName = "";
	private int printCount = 1; // 打印份数

	private String rwCode; // 二维码CODE

	public PrintController(String code, Stage primaryStage) {
		this.code = code;
		this.primaryStage = primaryStage;
		scene = new Scene(createContent3());
//		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		 scene.getStylesheets().add(getClass().getResource("application_print.css").toExternalForm());
		primaryStage.setTitle("提货宝");
		primaryStage.setFullScreen(true); // 全屏显示
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public AnchorPane getTableView(final int totalWidth, final int totalHeigh, final int floatWith) {
		String planDistDriver_uri = url + "qrcode/getPlanInfo?code=" + code;
		String planDistDriver_info = com.erdos.thb.tools.HttpClient.gethttpResponse(planDistDriver_uri);
		ResultData rd = null;
		PlanDistDriver pd = new PlanDistDriver();
		printCount = pd.getPrintCount() == null ? 1 : pd.getPrintCount();
		PlanOrder po = new PlanOrder();
		if (planDistDriver_info != null) {
			rd = JSON.toJavaObject(JSONArray.parseObject(planDistDriver_info), ResultData.class);
			if (rd.getCode().equals("1") && rd.getData() != null) {
				pd = rd.getData();
				if (pd != null) {
					po = pd.getPlanOrder();
				}
			}
		}

		int floatWith1 = 50;
		int floatWith2 = 8;
		int width1 = totalWidth / 5;
		int width2 = totalWidth / 3 + floatWith1;
		int width3 = (width1 + width2) / 4;
		int width4 = ((width1 + width2) * 2) / 6;

		int heigh = totalHeigh / 8;
		AnchorPane centerAP = new AnchorPane();
		// 货物信息
		String goodsInfoStr = po.getGoodsType() == null ? "" : StringUtils.isEmpty(po.getGoodsType().getLabel());
		String goodsNameStr = po.getGoodsName() == null ? "" : StringUtils.isEmpty(po.getGoodsName());
		String goodsModelStr = po.getGoodsModel() == null ? "" : StringUtils.isEmpty(po.getGoodsModel());
		String goodsSpecStr = po.getGoodsSpec() == null ? "" : StringUtils.isEmpty(po.getGoodsSpec());
		String goodsNumStr = po.getGoodsNum() == null ? "" : StringUtils.isEmpty(po.getGoodsNum());
		Text goodsInfo = new Text("货物信息： " + goodsNameStr + " " + goodsInfoStr + " " + goodsModelStr + " "
				+ goodsSpecStr + " " + goodsNumStr);
		goodsInfo.getStyleClass().add("infoFont");
		AnchorPane.setTopAnchor(goodsInfo, Double.valueOf(10));
		AnchorPane.setLeftAnchor(goodsInfo, Double.valueOf(0));
		String deliveryCompanyNameInfo = pd.getPlanOrder() == null ? ""
				: StringUtils.isEmpty(po.getReceiverCompanyName());
		// 托运单位
		Text company = new Text("托运单位：" + deliveryCompanyNameInfo);
		company.getStyleClass().add("infoFont");
		AnchorPane.setTopAnchor(company, Double.valueOf(heigh));
		AnchorPane.setLeftAnchor(company, Double.valueOf(0));
		// 合同编号
		String planCode = StringUtils.isEmpty(po.getPlanCode());
		rwCode = planCode;
		Text contractNo = new Text("合同编号：" + planCode);
		contractNo.getStyleClass().add("infoFont");
		AnchorPane.setTopAnchor(contractNo, Double.valueOf(heigh));
		AnchorPane.setLeftAnchor(contractNo, Double.valueOf(width1 + width2));

//		int floatHeigh = 0;
		/*------------------------------第一行-----------------------------------------*/
		Label l1 = new Label("甲方(承运单位)");
		l1.getStyleClass().add("t2");
		l1.setWrapText(true);
		l1.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l1, Double.valueOf(heigh * 2));
		AnchorPane.setLeftAnchor(l1, Double.valueOf(anchorPaneStartLeftWidth));
		companyName = StringUtils.isEmpty(po.getCopanyName());
		Label l2 = new Label(companyName);
		l2.getStyleClass().add("t3");
		l2.setWrapText(true);
		l2.setPrefWidth(width2);
		AnchorPane.setTopAnchor(l2, Double.valueOf(heigh * 2));
		AnchorPane.setLeftAnchor(l2, Double.valueOf(width1));

		Label l3 = new Label("乙方(承运单位)");
		l3.getStyleClass().add("t4");
		l3.setWrapText(true);
		l3.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l3, Double.valueOf(heigh * 2));
		AnchorPane.setLeftAnchor(l3, Double.valueOf(width1 + width2));
		Label l4 = new Label(StringUtils.isEmpty(pd.getDriverName()));
		l4.getStyleClass().add("t3");
		l4.setWrapText(true);
		l4.setPrefWidth(width2 + floatWith);
		AnchorPane.setTopAnchor(l4, Double.valueOf(heigh * 2));
		AnchorPane.setLeftAnchor(l4, Double.valueOf(width1 * 2 + width2));

		/*------------------------------第二行-----------------------------------------*/
		Label l5 = new Label("乙方证件号码");
		l5.getStyleClass().add("t5");
		l5.setWrapText(true);
		l5.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l5, Double.valueOf(heigh * 3));
		AnchorPane.setLeftAnchor(l5, Double.valueOf(anchorPaneStartLeftWidth));
		Label l6 = new Label(StringUtils.isEmpty(pd.getDriverLicense()));
		l6.getStyleClass().add("t3");
		l6.setWrapText(true);
		l6.setPrefWidth(width2);
		AnchorPane.setTopAnchor(l6, Double.valueOf(heigh * 3));
		AnchorPane.setLeftAnchor(l6, Double.valueOf(width1));
		Label l7 = new Label("车牌号");
		l7.getStyleClass().add("t6");
		l7.setWrapText(true);
		l7.setPrefWidth(width3);
		AnchorPane.setTopAnchor(l7, Double.valueOf(heigh * 3));
		AnchorPane.setLeftAnchor(l7, Double.valueOf(width1 + width2));
		Label l8 = new Label(StringUtils.isEmpty(pd.getPlateNo()));
		l8.getStyleClass().add("t7");
		l8.setWrapText(true);
		l8.setPrefWidth(width3);
		AnchorPane.setTopAnchor(l8, Double.valueOf(heigh * 3));
		AnchorPane.setLeftAnchor(l8, Double.valueOf(width1 + width2 + width3));
		Label l9 = new Label("承运人电话");
		l9.getStyleClass().add("t6");
		l9.setWrapText(true);
		l9.setPrefWidth(width3);
		AnchorPane.setTopAnchor(l9, Double.valueOf(heigh * 3));
		AnchorPane.setLeftAnchor(l9, Double.valueOf(width1 + width2 + (width3 * 2)));
		Label l10 = new Label(StringUtils.isEmpty(pd.getDriverMobile()));
		l10.getStyleClass().add("t7");
		l10.setWrapText(true);
		l10.setPrefWidth(width3 + floatWith2);
		AnchorPane.setTopAnchor(l10, Double.valueOf(heigh * 3));
		AnchorPane.setLeftAnchor(l10, Double.valueOf(width1 + width2 + (width3 * 3)));

		/*------------------------------第三行-----------------------------------------*/
		Label l11 = new Label("装货地址");
		l11.getStyleClass().add("t5");
		l11.setWrapText(true);
		l11.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l11, Double.valueOf(heigh * 4));
		AnchorPane.setLeftAnchor(l11, Double.valueOf(anchorPaneStartLeftWidth));
		StringBuffer dlsb = new StringBuffer("");
		if (po != null) {
			dlsb.append(po.getDeliveryCompanyAddressP() != null
					? StringUtils.isEmpty(po.getDeliveryCompanyAddressP().getValue()) : "");
			dlsb.append(po.getDeliveryCompanyAddressC() != null
					? StringUtils.isEmpty(po.getDeliveryCompanyAddressC().getValue()) : "");
			dlsb.append(po.getDeliveryCompanyAddressA() != null
					? StringUtils.isEmpty(po.getDeliveryCompanyAddressA().getValue()) : "");
			dlsb.append(po.getDeliveryCompanyAddressS() != null
					? StringUtils.isEmpty(po.getDeliveryCompanyAddressS().getValue()) : "");
			dlsb.append(StringUtils.isEmpty(po.getDeliveryCompanyAddressDetail()));
		}
		Label l12 = new Label(dlsb.toString());
		l12.getStyleClass().add("t3");
		l12.setWrapText(true);
		l12.setPrefWidth(width1 + width2 * 2 + floatWith);
		AnchorPane.setTopAnchor(l12, Double.valueOf(heigh * 4));
		AnchorPane.setLeftAnchor(l12, Double.valueOf(width1));

		Label l13 = new Label("卸货地址");
		l13.getStyleClass().add("t5");
		l13.setWrapText(true);
		l13.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l13, Double.valueOf(heigh * 5));
		AnchorPane.setLeftAnchor(l13, Double.valueOf(anchorPaneStartLeftWidth));
		StringBuffer rcsb = new StringBuffer("");
		if (po != null) {
			rcsb.append(po.getReceiverCompanyAddressP() == null ? ""
					: StringUtils.isEmpty(po.getReceiverCompanyAddressP().getValue()));
			rcsb.append(po.getReceiverCompanyAddressC() == null ? ""
					: StringUtils.isEmpty(po.getReceiverCompanyAddressC().getValue()));
			rcsb.append(po.getReceiverCompanyAddressA() == null ? ""
					: StringUtils.isEmpty(po.getReceiverCompanyAddressA().getValue()));
			rcsb.append(po.getReceiverCompanyAddressS() == null ? ""
					: StringUtils.isEmpty(po.getReceiverCompanyAddressS().getValue()));
			rcsb.append(StringUtils.isEmpty(po.getReceiverCompanyAddressDetail()));
		}
		Label l14 = new Label(rcsb.toString());
		l14.getStyleClass().add("t3");
		l14.setWrapText(true);
		l14.setPrefWidth(width1 + width2 * 2 + floatWith);
		AnchorPane.setTopAnchor(l14, Double.valueOf(heigh * 5));
		AnchorPane.setLeftAnchor(l14, Double.valueOf(width1));

		/*------------------------------第四行-----------------------------------------*/
		Label l15 = new Label("允许损耗磅差");
		l15.getStyleClass().add("t5");
		l15.setWrapText(true);
		l15.setPrefWidth(width4);
		AnchorPane.setTopAnchor(l15, Double.valueOf(heigh * 6));
		AnchorPane.setLeftAnchor(l15, Double.valueOf(anchorPaneStartLeftWidth));
		Label l16 = new Label(pd.getPlanOrder() != null ? po.getDeviation().toString() : "");
		l16.getStyleClass().add("t3");
		l16.setWrapText(true);
		l16.setPrefWidth(width4);
		AnchorPane.setTopAnchor(l16, Double.valueOf(heigh * 6));
		AnchorPane.setLeftAnchor(l16, Double.valueOf(width4));
		Label l17 = new Label("运价(元/吨)");
		l17.getStyleClass().add("t6");
		l17.setWrapText(true);
		l17.setPrefWidth(width4);
		AnchorPane.setTopAnchor(l17, Double.valueOf(heigh * 6));
		AnchorPane.setLeftAnchor(l17, Double.valueOf(width4 * 2));
		Label l18 = new Label(StringUtils.isEmpty(pd.getTransFee().toString()));
		l18.getStyleClass().add("t7");
		l18.setWrapText(true);
		l18.setPrefWidth(width4);
		AnchorPane.setTopAnchor(l18, Double.valueOf(heigh * 6));
		AnchorPane.setLeftAnchor(l18, Double.valueOf(width4 * 3));
		Label l19 = new Label("收货方电话");
		l19.getStyleClass().add("t4");
		l19.setWrapText(true);
		l19.setPrefWidth(width4);
		AnchorPane.setTopAnchor(l19, Double.valueOf(heigh * 6));
		AnchorPane.setLeftAnchor(l19, Double.valueOf(width4 * 4));
		Label l20 = new Label(pd.getPlanOrder() != null ? StringUtils.isEmpty(po.getReceiverLinkMobile()) : "");
		l20.getStyleClass().add("t4");
		l20.setWrapText(true);
		l20.setPrefWidth(((width1 * 2) + (width2 * 2) + floatWith) - (width4 * 5));
		AnchorPane.setTopAnchor(l20, Double.valueOf(heigh * 6));
		AnchorPane.setLeftAnchor(l20, Double.valueOf(width4 * 5));

		/*------------------------------第五行-----------------------------------------*/
		Label l21 = new Label("结算方式");
		l21.getStyleClass().add("t5");
		l21.setWrapText(true);
		l21.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l21, Double.valueOf(heigh * 7));
		AnchorPane.setLeftAnchor(l21, Double.valueOf(anchorPaneStartLeftWidth));
		Label l22 = new Label(StringUtils.isEmpty(pd.getAccountType()));
		l22.getStyleClass().add("t3");
		l22.setWrapText(true);
		l22.setPrefWidth(width2);
		AnchorPane.setTopAnchor(l22, Double.valueOf(heigh * 7));
		AnchorPane.setLeftAnchor(l22, Double.valueOf(width1));
		Label l23 = new Label("结算电话");
		l23.getStyleClass().add("t6");
		l23.setWrapText(true);
		l23.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l23, Double.valueOf(heigh * 7));
		AnchorPane.setLeftAnchor(l23, Double.valueOf(width1 + width2));
		Label l24 = new Label(StringUtils.isEmpty(pd.getLinkMobile()));
		l24.getStyleClass().add("t7");
		l24.setWrapText(true);
		l24.setPrefWidth(width2 + floatWith);
		AnchorPane.setTopAnchor(l24, Double.valueOf(heigh * 7));
		AnchorPane.setLeftAnchor(l24, Double.valueOf(width1 * 2 + width2));

		/*------------------------------第六行-----------------------------------------*/
		Label l25 = new Label("备注");
		l25.getStyleClass().add("t5");
		l25.setWrapText(true);
		l25.setPrefWidth(width1);
		AnchorPane.setTopAnchor(l25, Double.valueOf(heigh * 8));
		AnchorPane.setLeftAnchor(l25, Double.valueOf(anchorPaneStartLeftWidth));
		Label l26 = new Label(StringUtils.isEmpty(pd.getRemark()));
		l26.getStyleClass().add("t3");
		l26.setWrapText(true);
		l26.setPrefWidth(width2 + width1 + width2 + floatWith);
		AnchorPane.setTopAnchor(l26, Double.valueOf(heigh * 8));
		AnchorPane.setLeftAnchor(l26, Double.valueOf(width1));

		centerAP.getChildren().addAll(goodsInfo, company, contractNo, l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12,
				l13, l14, l15, l16, l17, l18, l19, l20, l21, l22, l23, l24, l25, l26);
		// centerAP.getChildren().addAll(l1, l2, l3, l4, l5, l6, l7, l8);
		return centerAP;
	}

	public Parent createContent2() {
		VBox bigBox = new VBox();
		bigBox.setAlignment(Pos.CENTER);
		BorderPane borderPane = new BorderPane();

		// 加入表格的布局
		BorderPane border1 = new BorderPane();
		VBox topVbox = new VBox();
		// Top content
		Text scenetitle = new Text(companyName + " 货物运输委托合同");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 30));
		topVbox.getChildren().add(scenetitle);
		topVbox.setPrefHeight(100);
		topVbox.setAlignment(Pos.CENTER);
		// BorderPane.setAlignment(scenetitle, Pos.TOP_CENTER);

		borderPane.setTop(topVbox);
		// Left content
		// Label label1 = new Label("Left hand");
		// Button leftButton = new Button("left");
		VBox leftVbox = new VBox();
		leftVbox.setPrefWidth(400);
		// leftVbox.getChildren().addAll(label1, leftButton);
		borderPane.setLeft(leftVbox);
		// Right content
		// Label rightlabel1 = new Label("Right hand");
		// Button rightButton = new Button("right");
		VBox rightVbox = new VBox();
		rightVbox.setPrefWidth(400);
		// rightVbox.getChildren().addAll(rightlabel1, rightButton);
		borderPane.setRight(rightVbox);

		// 信息和打印按钮布局
		BorderPane border2 = new BorderPane();
		border2.setRight(getPrintBut());
		// border2.setLeft(getInfo());
		border2.setPrefHeight(50);
		border1.setTop(border2);
		VBox centerVbox = new VBox();
		centerVbox.getChildren().add(getTableView(804, 320, 5));
		centerVbox.getChildren().add(getInfo());
		border1.setCenter(centerVbox);
		borderPane.setCenter(border1);
		// Bottom content
		// Label bottomLabel = new Label();

		// borderPane.setBottom(getInfo());
		bigBox.getChildren().add(borderPane);
		return bigBox;
	}

	public Parent createContent3() {
		// 加入表格的布局
		VBox topVbox = new VBox();
		// Top content
		Text scenetitle = new Text("内蒙古达象物流有限公司货物运输委托合同");
		scenetitle.getStyleClass().add("title");
		topVbox.getChildren().add(scenetitle);
		topVbox.setPrefHeight(50);
		topVbox.setPrefWidth(500);
		topVbox.setMaxWidth(500);
		topVbox.setAlignment(Pos.CENTER);
		// BorderPane.setAlignment(scenetitle, Pos.TOP_CENTER);

		printBorderPane.setTop(topVbox);
		VBox leftVbox = new VBox();
		leftVbox.setPrefWidth(0);
		printBorderPane.setLeft(leftVbox);
		VBox rightVbox = new VBox();
		rightVbox.setPrefWidth(0);
		printBorderPane.setRight(rightVbox);
		// 信息和打印按钮布局
		VBox centerVbox = new VBox();
		centerVbox.getChildren().add(getTableView(464, 188, 8));

		HBox nodeBox = new HBox();
		nodeBox.getChildren().add(getInfo());
		// Bottom content
		// Label bottomLabel = new Label();
		// 二维码图片生成
		ImageView imageView = new ImageView();
		try {
			imageView.setImage(QrCodeUtil.get(rwCode));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nodeBox.getChildren().add(imageView);
		centerVbox.getChildren().add(nodeBox);
		printBorderPane.setCenter(centerVbox);
		return printBorderPane;
	}

	public Parent getInfo() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER_LEFT);
		String[] strs = { "一、承运人必须准确验收货物包装、数量、吨位，按指定厂区装货。", "二、承运人必须备好绳、锁、篷布等货物防护用具。",
				"三、运输途中货物发生损害、丢失、雨淋、污染、超载卸货、货物亏吨、扣押货物、偷盗、抢劫、换货、汽车自燃及交",
				"通事故等造成的经济损失均由承运人自行全部承担。如出现偷盗，调换货物等弄虚作假行为除赔偿货物价值外，承运方", "需赔偿10万元违约金。",
				"四、运输中实际发生的过路费、加油费、副燃油、耗用的配件及轮胎费用，承运人需把全部单据、发票交回承运单位。",
				"五、承运人及车辆到达装货、卸货地点必须严格遵守厂方各项管制制度，如因为不遵守制度所造成的一切责任、损失、", "罚款，均由承运人承担。",
				"六、运费结算：车辆到达目的地，收货方验证货物，卸车后收货方出具签字单、过磅单等收货凭证。承运人在5日内将", "收货凭证交回给承运单位进行结算。",
				"七、运输起止日期为：    年    月    日至    年    月   日，若承运人未能按时将货物送到目的地而造成的一切损失和责",
				"任全部由承运人承担，并每超一天承担600元违约金。承运人不得以任何理由留置或扣押托运单位拉运货物，否则按货", "物价值的1.5倍向承运单位支付违约金并承担全部损失。",
				"八、争议解决方式：双方如有纠纷应及时友好协商解决，协商无效的，可向甲方所在地有管辖权的人民法院申请裁决。" };
		Text text = null;
		for (int i = 0; i < strs.length; i++) {
			text = new Text(strs[i].toString());
			text.getStyleClass().add("font");
			grid.add(text, 0, i);
		}
		return grid;
	}

	public Parent getPrintBut() {
		/*---------------------------------打印按钮---------------------------------*/
		Button btn = new Button("打印");
		btn.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.TOP_CENTER);
		hbBtn.getChildren().add(btn);
		hbBtn.setMaxHeight(300);
		hbBtn.prefHeight(300);
		hbBtn.setFillHeight(false);
		// 打印按钮点击事件
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println("............开始打印..............");
				Printer printer = Printer.getDefaultPrinter();
				PageLayout paper = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, MarginType.DEFAULT);
				PrinterJob job = PrinterJob.createPrinterJob();
				JobSettings jobSetting = null;
				if (job != null) {
					Scene scene = new Scene(createContent3());
					scene.getStylesheets().add(getClass().getResource("application_print.css").toExternalForm());
					jobSetting = job.getJobSettings();
					jobSetting.setCopies(printCount);
					job.printPage(paper, createContent3());
					// job.printPage(createContent3());
					// job.printPage(getTableView());
					job.endJob();
					// 获取MAC地址
					InetAddress ia;
					try {
						ia = InetAddress.getLocalHost();
						String mac = IntenetTool.getLocalMac(ia);
						// 向打印接口传递打印记录
						String planDistDriver_uri = url + "print/updPlanDistStatus?code=" + code+"&macAddress="+mac;
						com.erdos.thb.tools.HttpClient.gethttpResponse(planDistDriver_uri);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				primaryStage.close();
				new LoginController().start(primaryStage);
			}
		});
		return hbBtn;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene scene = new Scene(createContent2());
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		// scene.getStylesheets().add(getClass().getResource("application_print.css").toExternalForm());
		primaryStage.setFullScreen(true); // 全屏显示
		// Rectangle2D primaryScreenBounds =
		// Screen.getPrimary().getVisualBounds();
		// primaryStage.setX(primaryScreenBounds.getMinX());
		// primaryStage.setY(primaryScreenBounds.getMinY());
		// primaryStage.setWidth(primaryScreenBounds.getWidth());
		// primaryStage.setHeight(primaryScreenBounds.getHeight());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
