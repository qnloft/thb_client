package com.erdos.thb.controller;

import java.util.HashMap;

import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class StageController {

	//建立一个专门存储Stage的Map，全部用于存放Stage对象
    private HashMap<String, Stage> stages = new HashMap<String, Stage>();

    public StageController(){
    	loginStage();
    }

    private void loginStage(){
    	//构造对应的Stage
//        Scene tempScene = new Scene(new LoginController().loginPane());
        Stage tempStage = new Stage();
        tempStage.setTitle("提货宝");
        tempStage.setFullScreen(true); // 全屏显示
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		tempStage.setX(primaryScreenBounds.getMinX());
		tempStage.setY(primaryScreenBounds.getMinY());
		tempStage.setWidth(primaryScreenBounds.getWidth());
		tempStage.setHeight(primaryScreenBounds.getHeight());
//        tempStage.setScene(tempScene);
        //将设置好的Stage放到HashMap中
        this.addStage("login", tempStage);
    }
    
    
    /**
     * 将加载好的Stage放到Map中进行管理
     *
     * @param name  设定Stage的名称
     * @param stage Stage的对象
     */
    public void addStage(String name, Stage stage) {
        stages.put(name, stage);
    }


    /**
     * 通过Stage名称获取Stage对象
     *
     * @param name Stage的名称
     * @return 对应的Stage对象
     */
    public Stage getStage(String name) {
        return stages.get(name);
    }


    /**
     * 将主舞台的对象保存起来，这里只是为了以后可能需要用，目前还不知道用不用得上
     *
     * @param primaryStageName 设置主舞台的名称
     * @param primaryStage     主舞台对象，在Start()方法中由JavaFx的API建立
     */
    public void setPrimaryStage(String primaryStageName, Stage primaryStage) {
        this.addStage(primaryStageName, primaryStage);
    }


    /**
     * 加载窗口地址，需要fxml资源文件属于独立的窗口并用Pane容器或其子类继承
     *
     * @param name      注册好的fxml窗口的文件
     * @param resources fxml资源地址
     * @param styles    可变参数，init使用的初始化样式资源设置
     * @return 是否加载成功
     */
    public boolean loadStage(String name ,Parent obj, String Stylesheets) {
        try {
            //构造对应的Stage
            Scene tempScene = new Scene(obj);
            Stage tempStage = new Stage();
            tempStage.setTitle("提货宝");
            tempStage.setFullScreen(true); // 全屏显示
    		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    		tempStage.setX(primaryScreenBounds.getMinX());
    		tempStage.setY(primaryScreenBounds.getMinY());
    		tempStage.setWidth(primaryScreenBounds.getWidth());
    		tempStage.setHeight(primaryScreenBounds.getHeight());
            tempStage.setScene(tempScene);

            if(Stylesheets != null && !Stylesheets.equals(""))
            	tempScene.getStylesheets().add(getClass().getResource(Stylesheets).toExternalForm());
            //将设置好的Stage放到HashMap中
            this.addStage(name, tempStage);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 显示Stage但不隐藏任何Stage
     *
     * @param name 需要显示的窗口的名称
     * @return 是否显示成功
     */
    public boolean setStage(String name) {
        this.getStage(name).show();
        return true;
    }


    /**
     * 显示Stage并隐藏对应的窗口
     *
     * @param show  需要显示的窗口
     * @param close 需要删除的窗口
     * @return
     */
    public boolean setStage(String show, String close) {
        getStage(close).close();
        setStage(show);
        return true;
    }


    /**
     * 在Map中删除Stage加载对象
     *
     * @param name 需要删除的fxml窗口文件名
     * @return 是否删除成功
     */
    public boolean unloadStage(String name) {
        if (stages.remove(name) == null) {
            System.out.println("窗口不存在，请检查名称");
            return false;
        } else {
            System.out.println("窗口移除成功");
            return true;
        }
    }
}
