package com.erdos.thb.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.erdos.thb.model.LoginResultData;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class LoginController extends Application{

	final String url = "";
	
	final Button btn = new Button();
	final Text actiontarget = new Text();
	Stage primaryStage;
	Scene scene;
	
	public VBox loginPane(){
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

//        Text scenetitle = new Text("自动取票机");
//        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
//        grid.add(scenetitle, 0, 0, 2, 1);

//        Label userName = new Label("输入取单码:");
//        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        userTextField.setPrefWidth(392);
        userTextField.setPrefHeight(65);
        grid.add(userTextField, 0, 1);

        
        Background btnBackGround = new Background(new BackgroundImage(new Image(this.getClass().getResourceAsStream("btn_text.jpg")), null, null, null, null));
        btn.setBackground(btnBackGround);
        btn.setPrefWidth(205);
        btn.setPrefHeight(69);
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        ProgressIndicator pi = new ProgressIndicator();
        grid.add(hbBtn, 1, 1);

        actiontarget.setFont(Font.font("Tahoma", FontWeight.BLACK, 20));
        grid.add(actiontarget, 0, 3);
//        grid.setColumnSpan(actiontarget, 2);
//        grid.setHalignment(actiontarget, RIGHT);
        actiontarget.setId("actiontarget");

        VBox bg = new VBox();
        bg.setAlignment(Pos.CENTER);
        Background boxBackGround = new Background(new BackgroundImage(new Image(this.getClass().getResourceAsStream("bg.jpg")), null, null, null, null));
        bg.setBackground(boxBackGround);
        bg.getChildren().add(grid);
        
        //输入框回车事件
        userTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER){
//                    showHistoryWords(querybox.getText());
                	changeOnClick(grid, userTextField, hbBtn, pi);
                }
            }
        });
        
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
            	changeOnClick(grid, userTextField, hbBtn, pi);
            	
            }

        });
		return bg;
	}
	private void changeOnClick(GridPane grid, TextField userTextField, HBox hbBtn, ProgressIndicator pi) {
		showPi(grid, hbBtn, pi);
    	if(userTextField.getText() != null && !userTextField.getText().equals("")){
    		String uri = url + "?code=" + userTextField.getText();
        	String result = com.erdos.thb.tools.HttpClient.gethttpResponse(uri);
        	if(result != null){
        		JSONObject resultJson = JSONArray.parseObject(result);
        		LoginResultData resultData = JSON.toJavaObject(resultJson, LoginResultData.class);
				if(resultData.getMessage() == null && resultData.getData() != null){
					primaryStage.close();
//					primaryStage.hide();
            		primaryStage.setFullScreen(true);	//全屏显示
            		try {
//            			scene = new Scene(new PrintController(userTextField.getText()).createContent2());
//            			primaryStage.setScene(scene);
            			new PrintController(userTextField.getText() ,primaryStage);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText(resultData.getMessage());
                    showBtn(grid, hbBtn, pi);
				}
        	}else{
        		actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("服务器无法连接！");
                showBtn(grid, hbBtn, pi);
        	}
    	}else{
    		actiontarget.setFill(Color.FIREBRICK);
            actiontarget.setText("请输入正确单号！");
            showBtn(grid, hbBtn, pi);
    	}
	}
	
	private void showPi(GridPane grid, HBox hbBtn, ProgressIndicator pi) {
		grid.getChildren().remove(hbBtn);
    	grid.add(pi, 1, 1);
	}
	
	private void showBtn(GridPane grid, HBox hbBtn, ProgressIndicator pi) {
		grid.getChildren().remove(pi);
    	grid.add(hbBtn, 1, 1);
	}
	@Override
    public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		scene = new Scene(loginPane());
		primaryStage.setTitle("提货宝");
		primaryStage.setFullScreen(true); // 全屏显示
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		primaryStage.setX(primaryScreenBounds.getMinX());
		primaryStage.setY(primaryScreenBounds.getMinY());
		primaryStage.setWidth(primaryScreenBounds.getWidth());
		primaryStage.setHeight(primaryScreenBounds.getHeight());
		primaryStage.setScene(scene);
		primaryStage.show();
    }

	public static void main(String[] args) {
		launch(args);
	}
}
